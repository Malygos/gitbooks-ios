# Xcode 菜单栏

![menu](./img/menu.png)

###### Xcode

```
About Xcode（关于 Xcode 的版本信息、描述、致谢和许可证）
Xcode Extensions（Xcode 扩展，会跳转到 App Store，可以看到很多扩展程序，有一部分是收费的）
---
Preferences...（❗ Xcode 偏好设置）
Behaviors
  ...
---
Xcode Server...
---
Open Developer Tool（❗ 开发工具）
  Instruments：调试工具
  Simulator：模拟器
  Accessibility Inspector：分析控件层级结构
  File Merge：文件差异合并工具
  Create ML：用来创建机器学习模型
  Reality Composer：AR工具
Services
---
Hide Xcode（隐藏Xcode窗口）
Hide Others（隐藏除Xcode外的其他窗口）
Show All（显示其他窗口）
---
Quit Xcode（退出Xcode）
```

推荐快捷键：

- ✅ Preferences，打开偏好设置，快捷键：command+,
- ✅ Quit Xcode，关闭Xcode，快捷键：command+q



#### File

```
New（新建）
    Editor（新建编辑窗，也就是写代码的窗口）
    Editor Below（在下侧新建编辑窗）
    Window Tab（新建窗口选项卡）
    Window（新建窗口，也就是整个 Xcode 窗口）
    ---
    File...（新建文件）
    Target...（新建 Target，等同于在 xcodeproj 中的 TARGETS 中新建）
    ---
    Playground...（新建 playground）
    Project...（新建工程）
    Swift Package...（新建 swift 包）
    Workspace...（新建工作空间）
    ---
    Group（新建组，组就是项目目录中的虚拟文件夹，实际文件路径不存在该文件夹）
    Group from Selection（将选中文件/文件夹放入新建的组中）
---
Add Files to "ProjectName"...（添加指定文件到工程）
---
Open...（打开文件/文件夹）
Open Recent（打开最近文件/文件夹）
Open Quickly...（快捷搜索并打开文件/文件夹）
---
Close Tab（关闭编辑窗中的文件标签，等同于在编辑窗文件标签上右键关闭）
Close "file.ex"（关闭编辑窗中当前的文件标签）
Close Editor（关闭编辑窗）
Close Window（关闭Xcode窗口，整个Xcode窗口都会关闭）
Close Workspace（关闭工作空间，整个工作空间都会关闭）
---
Save（保存）
Duplicate...（复制文件）
Revert to Saved...（撤销保存）
Unlock...（解锁文件，比如 pod 引入的文件会上锁，同编辑窗右上角点击小图标解锁）
Export...
---
Show in Finder（在 Finder 中显示文件）
Open in Tab（在新tab中打开文件）
Open in New Window（✅ 在新window中打开文件，在文件上右键也有该操作选项）
Open with External Editor
---
Swift Packages
		Add Package Dependency...
		---
    Reset Package Caches
    Resolve Package Versions
    Update to Latest Package Versions
---
Save As Workspace...
Workspace Settings...（❗ 工作空间设置）
---
Page Setup...
Print...（打印）
```

推荐快捷键：

- ✅ New File，新建文件，快捷键：command+n
- ✅ Add Files to "ProjectName"，添加指定文件到工程，快捷键：command+alt+a
- ✅ Open Quickly，快捷搜索并打开文件/文件夹，快捷键：command+shift+o
- ✅ Close Tab，关闭编辑窗中的文件标签，快捷键：command+w
- ✅ Save，保存，快捷键command+s
- ✅ Show in Finder，在Finder中显示文件，快捷键control+s



### Edit

```
Undo（撤销操作）
Redo（反撤销操作）
---
Cut（剪切）
Copy（拷贝）
Paste（粘贴）
Paste Special
Paste and Preserve Formatting
Duplicate
Delete（删除）
---
Select All（选中所有）
---
Sort（对项目导航栏中选中的文件目录进行排序）
    By Name（✅ 按文件名排序）
    By Type（按文件类型排序）
Format
    Show Fonts
    Show Colors
    ---
		Font
		Text
		Spelling and Grammar
		Substitutions
		Transformations
				Make Upper Case（✅ 选中代码格式化为纯大写）
				Make Lower Case（✅ 选中代码格式化为纯小写）
				Capitalize（✅ 选中代码格式化为帕斯卡命名法，也叫大驼峰式）
		Speech
---
Convert
		To Current Swift Syntax...
    To Modern Objective-C Syntax...
    To Objective-C ARC...
---
Start Dictation...（启动听写功能）
Emoji & Symbols（✅ 输入Emoji和特殊符号）
```

推荐快捷键：

- ✅ Undo，撤销操作，快捷键command+z
- ✅ Redo，反撤销操作，快捷键command+shift+z
- ✅ Cut，剪切，快捷键command+x
- ✅ Copy，拷贝，快捷键command+c
- ✅ Paste，粘贴，快捷键command+v
- ✅ Select All，选中所有，快捷键command+a
- ❎ Sort By Name，按文件名排序，推荐自定义快捷键：alt+s
- ❎ Emoji & Symbols，输入Emoji和特殊符号，快捷键：command+control+Space



### View

```
Editor（编辑窗，也就是 window 中心区域用来写代码的窗口）
    Focus（仅关注当前文件所在的Editor，其他Editor将会隐藏，可点击Editor左上角的“展开”按钮展开所有Editor，仅多个Editor时可用）
    ---
		Show Related Items（显示 Related Items，见“Xcode 菜单栏”中关于 Editor 的介绍，快捷键：control+1）
		Show Previous History（显示 Previous History，见“Xcode 菜单栏”中关于 Editor 的介绍，快捷键：control+2）
		Show Next History（显示 Next History，见“Xcode 菜单栏”中关于 Editor 的介绍，快捷键：control+3）
		Show Top Level Items（显示 Top Level Items，见“Xcode 菜单栏”中关于 Editor 的介绍，快捷键：control+4）
		Show Group Files（显示 Group Files，见“Xcode 菜单栏”中关于 Editor 的介绍，快捷键：control+5）
		Show Document Items（显示 Document Items，见“Xcode 菜单栏”中关于 Editor 的介绍，快捷键：control+6）
Show Code Review（✅ 显示代码审查，等同于点击 Toolbar 右侧的 🔁 按钮）
---
Change Editor Orientation（改变多个Editor的排列方向，仅多个Editor时生效）
Rest Editor Sizes（重置Editor的大小，仅多个Editor时生效）
Reset Assistant Selection
---
Navigators（导航栏，也就是 window 左侧的窗口）
    Project（切换到项目导航栏，快捷键：command+1）
    Source Control（切换到资源控制导航栏，快捷键：command+2）
    Symbols（切换到符号导航栏，快捷键：command+3）
    Find（切换到搜索导航栏，快捷键：command+4）
    Issues（切换到问题导航栏，快捷键：command+5）
    Tests（切换到测试导航栏，快捷键：command+6）
    Debug（切换到调试导航栏，快捷键：command+7）
    Breakpoints（切换到断点导航栏，快捷键：command+8）
    Reports（切换到报告导航栏，快捷键：command+9）
    ---
    Hide Navigator（✅ 隐藏导航栏）
    Filter in Navigator（在导航栏中搜索，就是导航栏底部的过滤器）
Debug Area（调试区，也就是 window 下侧的窗口）
    Activate Console（控制台获得焦点）
    Hide Debug Area（✅ 隐藏调试区）
Inspectors（检查器，也就是 window 右侧的窗口）
    File（切换到检查器的文件标签）
    History（切换到检查器的历史标签）
    Quick Help（切换到检查器的快速帮助标签）
    Show Inspector（✅ 显示检查器）
Show Library（✅ 显示资源库，等同于点击 window 右上角的“+”按钮）
---
Hide Toolbar（✅ 隐藏工具栏，工具栏也就是 window 最顶部的那一栏）
Show Window Tab Bar（显示标签栏，也就是 window 顶部的标签栏）
Show All Window Tabs（在窗口中分块显示标签栏中的所有标签，效果有点类似于“四指”上推手势）
---
Enter Full Screen（全屏/恢复展示 window）
```



### Find

```
Find in Workspace...（在搜索导航栏中指定 workspace 范围进行搜索）
Find and Replace in Workspace...（在搜索导航栏中指定 workspace 范围进行搜索替换）
Find Next in Workspace（寻找下一个搜索结果，等同于在搜索导航栏结果列表中点击下一个）
Find Previous in Workspace（寻找上一个搜索结果，等同于在搜索导航栏搜索结果列表中点击上一个）
---
Find Selected Text in Workspace（在搜索导航栏中搜索当前选中的内容）
Find Selected Symbol in Workspace（在搜索导航栏中搜索当前选中的符号，焦点在符号上即可）
Find Call Hierarchy（✅ 寻找方法被调用的层级，类似功能可在编辑窗点击左上角按钮出现的菜单中查看Callers）
Find in Selected Groups...
---
Find...（在编辑窗中查找）
Find and Replace...（在编辑窗中查找替换）
Find Next（定位到下一个搜索结果）
Find Previous（定位到上一个搜索结果）
Find and Select Next（搜索并选中下一个搜索结果）
Find and Select Previous（搜索并选中上一个搜索结果）
---
Replace（在搜索后确认替换，等同于点击搜索bar上的“replace”按钮）
Replace All（在搜索后确认替换所有，等同于点击搜索bar上的“all”按钮）
Replace and Find Next（进行替换并查找下一个）
Replace and Find Previous（进行替换并查找上一个）
---
Use Selection for Find（将选择的内容填充到搜索bar的搜索输入框中）
Use Selection for Replace（将选择的内容填充到搜索bar的替换输入框中）
Select Next Occurrence
Select Previous Occurrence
---
Select All Find Matches
Select Find Matches in Selection
---
Hide Find Bar（隐藏搜索Bar）
```

推荐快捷键：

- ✅ Find in Workspace，指定 workspace 范围进行搜索，快捷键：command+shift+f

- ❎ Find and Replace in Workspace，指定 workspace 范围进行搜索替换，快捷键：command+shift+alt+f

- ❎ Find Selected Symbol in Workspace，搜索当前选中的符号，快捷键：command+control+shift+f

- ✅ Find，在编辑窗中查找，快捷键command+f

- ❎ Find and Replace，在编辑窗中查找替换，快捷键：command+alt+f

- ❎ Find Next，定位到下一个搜索结果，快捷键：command+g

- ❎ Find Previous，定位到上一个搜索结果，快捷键：command+shift+g

  

### Navigate

```
Reveal in Project Navigator（在项目导航栏中定位当前文件位置）
Reveal in Symbol Navigator（在符号导航栏中定位符号位置）
Reval in Debug Navigator
Open in Next Editor（在新开的编辑窗中打开文件）
Open in...（在指定位置新开编辑窗打开文件）
---
Move Focus to Next Area（移动光标焦点）
Move Focus to Next Editor（移动光标焦点）
Move Focus to Editor...（移动光标焦点）
---
Show Previous Tab（显示前一个文件Tab）
Show Next Tab（显示下一个文件Tab）
---
Go Forward（显示下一个编辑窗窗口，等同于在编辑窗中使用“双指”向右滑动的手势）
Go Back（显示前一个编辑窗窗口，等同于在编辑窗中使用“双指”向左滑动的手势）
---
Jump to Selection（将选中行滚动至窗口中间）
Jump to Definition（将窗口切换到符号的定义/实现的位置）
Jump to Original Source/Generated Interface
Jump to Last Destination
---
Jump to Next Issue（在“Issue navigator”中跳转到下一个错误/警告）
Jump to Previous Issue（在“Issue navigator”中跳转到上一个错误/警告）
---
Jump to Instruction Pointer
---
Jump to Next Counterpart
Jump to Previous Counterpart
Jump to Line in "file.ex"（跳转到当前文件指定行）
---
Jump to Next Placeholder
Jump to Previous Placeholder
---
Jump to Next Change（跳转到当前文件的下一个改动行）
Jump to Previous Change（跳转到当前文件的上一个改动行）
```

推荐快捷键：

- ❎ Show Previous Tab，显示前一个文件Tab，快捷键：command+{
- ❎ Show Next Tab，显示下一个文件Tab，快捷键：command+}
- ❎ Jump to Definition，将窗口切换到符号的定义/实现的位置，快捷键command+control+j
- ❎ Jump to Line in "file.ex"，跳转到当前文件指定行，快捷键command+l
- ❎ Jump to Next Change，跳转到当前文件的下一个改动行，快捷键command+\
- ❎ Jump to Previous Change，跳转到当前文件的上一个改动行，快捷键command+|



### Editor（xcodeproj）

```
Add Target...
---
Add Configuration
Add Capability
Add Build Setting
Add Build Phase
Add Build Rule
---
Add Localization
Export for Localization...
Import Localizations...
---
Validate Settings...
---
Show Setting Names
Show Definitions
```



### Editor（file）

```
Show Editor Only
---
Canvas
Assistant
Layout
---
Show Completions
Show Code Actions
Edit All in Scope
Refactor
---
Fix All Issues
Show Issue
Issues
---
Create Preview
Create Library Item
---
Selection
Structure
Code Folding
Syntax Coloring
Font Size
Theme
---
Minimap
Authors
Code Coverage
---
Invisibles
---
Show Last Change For Line
Create Code Snippet
```



### Product

```
Run（✅ 运行，等同于点击 Toolbar 左侧的“▶”按钮）
Test（测试，用于执行单元测试和UI测试）
Profile
Analyze（✅ 分析，Xcode会帮助分析代码中可能存在错误的地方，比如内存泄漏、循环引用、野指针等问题）
Archive（✅ 归档，用于打包ipa文件）
---
Build For（根据目的去进行编译）
    Running
    Testing
    Profiling
Perform Action
    ...
---
Build（✅ 编译）
Clean Build Folder（✅ 清理编译文件夹，对应Xcode工程目录树中的Products文件夹，真实路径为～/Library/Developer/Xcode/DerivedData/ProjName-xxxx/Build/）
Stop（✅ 停止运行/编译/...，等同于点击 Toolbar 左侧的“⏹ ”按钮）
---
Scheme
    Choose Scheme（✅ 展开Scheme窗口）
    Select Next Scheme
    Select Previous Scheme
    ---
    Edit Scheme...（编辑Scheme，等同于在Scheme窗口点击“Edit Scheme...”）
    New Scheme...（新建Scheme，等同于在Scheme窗口点击“New Scheme...”）
    Manage Schemes...（管理Scheme，等同于在Scheme窗口点击“Manage Scheme...”）
    ---
    Convert Scheme to use Test Plans...
Destination
    Choose Destination（展开目标列表，也就是要运行的设备列表）
    Select Next Destination（选择下一个运行设备）
    Select Previous Destination（选择上一个运行设备）
Test Plan
    ...
---
Create Bot...
```

推荐快捷键：

- ✅ Run，运行，快捷键command+r
- ✅ Build，编译，快捷键command+b
- ✅ Clean Build Folder，清理编译文件夹，快捷键command+shift+k
- ✅ Stop，停止运行/编译/...，快捷键command+.



### Debug

```
Pause
Continue To Currnet Line
Step Over
Step Into
Step Out
Step Over Instruction
Step Over Thread
Step Into Instruction
Step Into Thread
---
Deactivate Breakpoints
Breakpoints
---
Debug Wrokflow
---
Attach to Process by PID or Name...
Attach to Process
Detach from 'ProjectName'
---
Capture GPU Frame
Capture GPU Scope
GPU Overrides
Simulate Location
Simulate Background Fetch
Simulate MetricKit Payloads
Simulate UI Snapshot
View Debugging
---
StoreKit
```



### Source Control

```
Commit...
Push...
Pull...
Fetch Changes
Refresh File Status
---
Cherry-Pick...
Stash Changes...
Discard All Changes...
---
Add Selected Files
Discard Changes in Selected Files...
Mark Selected Files as Resolved
---
New Git Repositories...
---
Clone...
```



### Window

```
Minimize（最小化）
Zoom（全屏/恢复）
Tile Window to Left of Screen（把窗口当作瓦片展示在屏幕左侧）
Tile Window to Right of Screen（把窗口当作瓦片展示在屏幕右侧）
---
Rename Window Tab...（）
Show previous Window Tab
Show Next Window Tab
Move Tab to New Window
Merge All Windows
---
Developer Documentation（✅ 开发文档）
Welcome to Xcode（Xcode冷启动时的欢迎界面）
Devices and Simulators（❗ 管理设备和模拟器）
Organizer（✅ 分析和诊断）
---
Touch Bar
    Show Touch Bar（显示虚拟MultiTouchBar）
    ---
    Touch Bar (1st generation)（带esc按钮）
    Touch Bar (2nd generation)（不带esc按钮）
---
Bring All to Front（将所有Xcode窗口置于最前）
---
```

推荐快捷键：

- ❎ Developer Documentation，✅ 开发文档，快捷键command+shift+0

  

### Help

```
Search（搜索菜单中的功能）
---
Developer Documentation（✅ 开发文档，✅ 快捷键command+shift+0）
Human Interface Guidelines（人机界面指南，会去浏览器打开网页）
---
Xcode Help（帮助）
What`s New in Xcode（✅ 最新版Xcode介绍，会去浏览器打开网页）
Release Notes（✅ 发布日志，会去浏览器打开网页）
Developer Account Help（✅ 开发者账户帮助，会去浏览器打开网页）
App Store Connect Help（✅ App Store Connect帮助，会去浏览器打开网页）
Swift Programming Language Book（✅ Swift开发指南，会去浏览器打开网页）
Report an Issue（✅ 报告错误，会去浏览器打开网页）
---
Show Quick Help for Selected Item
Search Documentation for Selected Text
```

