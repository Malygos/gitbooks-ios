# Xcode 窗口

- Window
- Toolbar
- Navigator
- Inspector
- Library
- Debug Area

### Window

![window](./img/window.png)

- Toolbar：工具栏，顶部红色区域
- Window Tab Bar：窗口标签栏，顶部棕色区域
- Navigator：导航栏，左侧黄色区域
- Editor：编辑窗，中间绿色区域
- Debug Area：调试区，底部蓝色区域
- Inspector：检查器，右侧橙色区域
- Library：资源库，右上角桃红色区域第一个按钮，<img src="./img/toolbar_icon_library.png" style="zoom:50%;" />
- Core Review：代码审查，右上角桃红色区域第二个按钮，<img src="./img/toolbar_icon_code_review.png" style="zoom:50%;" />



### Toolbar

![toolbar](./img/toolbar.png)

从左到右依次如下：

- 展开/缩回导航栏（Navigator）
- 运行程序，<img src="./img/toolbar_icon_run.png" style="zoom:50%;" />
- 终止程序，<img src="./img/toolbar_icon_stop.png" style="zoom:50%;" />
- Scheme
- 执行目标（Destination）
- 状态栏（Statusbar）
- 资源库（Library），<img src="./img/toolbar_icon_library.png" style="zoom:50%;" />
- 代码审查（Code Review），<img src="./img/toolbar_icon_code_review.png" style="zoom:50%;" />
- 展开/缩回检查器（Inspector）



### Navigator

![navigator](./img/navigator.png)

从左到右依次如下：

- Project navigator：项目导航栏，展示项目文件目录
- Source Control navigator：源代码管理导航栏，展示通过源码方式引入工程的三方库，pod通过path方式引入的那些库
- Symbol navigator：符号导航栏，展示符号信息，符号包括：类Class、协议Protocol、属性property、方法function、结构体struct、union共用体、enum枚举、type变量类型、variable变量、global全局常量
- Find navigator：搜索导航栏，用于搜索/替换内容，可以设定搜索范围和匹配条件
- Issue navigator：问题导航栏，展示编译阶段产生的警告和错误
- Test navigator：测试导航栏，展示单元测试和UI测试，显示执行情况
- Debug navigator：调试导航栏，展示运行时的CPU、内存、IO、网络情况，断点调试时展示线程和调用栈信息
- Breakpoint navigator：断点导航栏，展示所有添加的断点，可以添加一些特定类型的断点和自定义断点
- Report navigator：报告导航栏，展示编译过程信息，展示警告和错误



### Editor

- Related Items

- Previous/Next History

- Top Level Items

- Group Files

- Document Items

  

###### Related Items

![editor_related_items](./img/editor_related_items.png)



###### Previous/Next History

![editor_previous_history](./img/editor_previous_history.png)



###### Top Level Items

![editor_top_level_items](./img/editor_top_level_items.png)



###### Group Files

![editor_group_files](./img/editor_group_files.png)



###### Document Items

![editor_document_items](./img/editor_document_items.png)



### Inspector

![inspector](./img/inspector.png)

- File Inspector，文件审查器，展示文件信息（如文件名、类型、路径等）和文件设置
- History Inspector，历史审查器，展示文件历史修改记录
- Quick Help Inspector，快速帮助审查器，展示帮助文档



### Library

![libarary](./img/library.png)



- ![](./img/library_icon_snippets.png)，Snippets，代码片段，比如输入 if 时 Xcode 会联想出很多模板，选择其中一个会自动键入模板代码。也可以自定义一些常用的模板，比如@property (nonatomic, strong) <#type#> *<#description#>
- ![](./img/library_icon_media.png)，Media，会展示项目中所有的多媒体资源，如图片，GIF等，下列内容不会展示：字体文件，webp文件
- ![](./img/library_icon_color.png)，Color，



### Debug Area

![debugarea](./img/debugarea.png)



- Debug Area Toolbar，调试区工具栏
  - ![](./img/debugarea_icon_hide.png)，显示/隐藏调试区
  - ![](./img/debugarea_icon_breakpoint.png)，断点
  - ![](./img/debugarea_icon_continue.png)，继续执行程序
  - ![](./img/debugarea_icon_stepover.png)，跳过断点
  - ![](./img/debugarea_icon_stepout.png)，单步进入
  - ![](./img/debugarea_icon_stepinto.png)，单步退出
  - ![](./img/debugarea_icon_hierarchy.png)，显示当前视图层级
  - ![](./img/debugarea_icon_memory.png)，内存图
  - ![](./img/debugarea_icon_environment.png)，环境配置，可设置 Light/Dark 模式，系统字体大小等
  - ![](./img/debugarea_icon_location.png)，模拟定位
- Variables View，变量视图，断点调试时显示变量值
- Console，控制台，打印日志和调试信息

